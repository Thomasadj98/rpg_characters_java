package thomasadj98.exceptions;

public class InvalidWeaponException extends Exception  {
    public InvalidWeaponException(String message) {
        super(message);
    }
}

