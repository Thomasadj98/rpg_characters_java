package thomasadj98.exceptions;

public class InvalidArmorException extends Exception{
    public InvalidArmorException(String message) {
        super(message);
    }
}

