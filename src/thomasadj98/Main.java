package thomasadj98;

import thomasadj98.character.*;
import thomasadj98.exceptions.InvalidWeaponException;
import thomasadj98.item.Item;
import thomasadj98.item.Weapon;
import thomasadj98.item.WeaponAttributes;

public class Main {

    public static void main(String[] args) throws InvalidWeaponException {
	// write your code here
        Ranger warriorMan = new Ranger("Peter");
        //System.out.println(mageMan.getName());
        warriorMan.displayStats();
        warriorMan.levelUp(3);
        warriorMan.displayStats();

        Weapon newWeapon = new Weapon(
                "gang",
                1,
                Item.Slot.WEAPON_SLOT,
                Weapon.weaponType.SWORD_WEAPON,
                new WeaponAttributes(
                        7,
                        1
                )
        );

        warriorMan.equip(newWeapon);

        System.out.println(newWeapon.getWeaponAttributes().getDamage());
    }
}
