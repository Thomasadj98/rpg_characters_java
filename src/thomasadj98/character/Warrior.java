package thomasadj98.character;

import thomasadj98.exceptions.InvalidArmorException;
import thomasadj98.exceptions.InvalidWeaponException;
import thomasadj98.item.Armor;
import thomasadj98.item.Item;
import thomasadj98.item.Weapon;

public class Warrior extends Character {

    public Warrior(String name) {
        super(name, 5, 2, 1, 10);
        System.out.println("Warrior created successfully!");

        PrimaryAttributes incrementPrimaryAttributes = new PrimaryAttributes(5, 3, 2, 1);
        this.setIncrementPrimaryAttributes(incrementPrimaryAttributes);
    }

    @Override
    public String equip(Weapon weapon) throws InvalidWeaponException {
        if (weapon.getLevelToEquip() > this.getLevel()) {
            throw new InvalidWeaponException("Your Character's level is not high enough.");
        }
        if (weapon.getWeaponType() != Weapon.weaponType.AXE_WEAPON && weapon.getWeaponType() != Weapon.weaponType.HAMMER_WEAPON && weapon.getWeaponType() != Weapon.weaponType.SWORD_WEAPON) {
            throw new InvalidWeaponException("Your Character can't equip these type(s) of weapon(s).");
        }
        equipment.put(Item.Slot.WEAPON_SLOT, weapon);

        return "New weapon equipped!";
    }

    @Override
    public String equip(Armor armor) throws InvalidArmorException {
        if (armor.getLevelToEquip() > this.getLevel()) {
            throw new InvalidArmorException("Your Character's level is not high enough to equip this type of armor.");
        }
        if (armor.getArmorType() != Armor.armorType.MAIL_ARMOR && armor.getArmorType() != Armor.armorType.PLATE_ARMOR) {
            throw new InvalidArmorException("Your Character can't equip this type of armor.");
        }

        equipment.put(armor.getItemSlot(), armor);

        return "New armor equipped!";
    }

    @Override
    public void calculateDPS() {

    }
}
