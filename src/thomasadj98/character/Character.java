package thomasadj98.character;

import thomasadj98.exceptions.*;
import thomasadj98.item.*;

import java.util.Hashtable;


// Character is super class for Mage, Ranger, Rogue and Warrior.
public abstract class Character {

    // Setting class properties
    private String name;
    private int level;
    private double dps;
    private PrimaryAttributes basePrimaryAttributes;
    private PrimaryAttributes incrementPrimaryAttributes;
    private SecondaryAttributes baseSecondaryAttributes;
    protected Hashtable<Item.Slot, Item> equipment;

    // Constructor to create basics of a character
    public Character(String name, int strength, int dexterity, int intelligence, int vitality) {
        this.name = name;
        this.level = 1;
        this.equipment = new Hashtable<Item.Slot, Item>();
        this.basePrimaryAttributes = new PrimaryAttributes(strength, dexterity, intelligence, vitality);
    }


    // Abstract methods
    public abstract String equip(Weapon weapon) throws InvalidWeaponException;

    public abstract String equip(Armor armor) throws InvalidArmorException;

    public abstract void calculateDPS();


    // Level Up Character
    public void levelUp(int levels) {
        if (levels > 0) {
            this.level += levels;
            setLevel(this.level);

            for (int i = 0; i < levels; i++) {
                PrimaryAttributes basePrimaryAttributes = getBasePrimaryAttributes();
                PrimaryAttributes incrementPrimaryAttributes = getIncrementPrimaryAttributes();

                basePrimaryAttributes.add(incrementPrimaryAttributes);
            }
        } else {
            throw new IllegalArgumentException();
        }
    }


    // Calculate methods

    // Calculate Secondary Attributes based on the Primary Attributes
    public SecondaryAttributes calculateSecondaryStats() {

        // Replace Primary Attributes with total

        return new SecondaryAttributes (
                this.basePrimaryAttributes.getVitality() * 10,
                this.basePrimaryAttributes.getStrength() - this.basePrimaryAttributes.getDexterity(),
                this.basePrimaryAttributes.getIntelligence()
        );
    }

    public void calculateWeaponBonus() {
        Item weaponEquipped = equipment.get(Item.Slot.WEAPON_SLOT);
        System.out.println(weaponEquipped);
    }

    // Calculate all stats of Character
    public void calculateAllStats() {
        this.baseSecondaryAttributes = calculateSecondaryStats();
    }


    // Display Character stats to the console
    public void displayStats() {
        calculateAllStats();

        StringBuilder stats = new StringBuilder("Stats:");

        stats.append("\n");
        stats.append("Character name: " + this.name);
        stats.append("\n");
        stats.append("Character level: " + this.level);
        stats.append("\n");
        stats.append("Strength: " + this.basePrimaryAttributes.getStrength());
        stats.append("\n");
        stats.append("Dexterity: " + this.basePrimaryAttributes.getDexterity());
        stats.append("\n");
        stats.append("Intelligence: " + this.basePrimaryAttributes.getIntelligence());
        stats.append("\n");
        stats.append("Vitality: " + this.basePrimaryAttributes.getVitality());
        stats.append("\n");
        stats.append("Health: " + this.baseSecondaryAttributes.getHealth());
        stats.append("\n");
        stats.append("Armor Rating: " + this.baseSecondaryAttributes.getArmorRating());
        stats.append("\n");
        stats.append("Elemental Resistance: " + this.baseSecondaryAttributes.getElementalResistance());

        System.out.println(stats.toString());
    }


    // GETTERS & SETTERS

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getLevel() {
        return level;
    }

    public void setLevel(int level) {
        this.level = level;
    }

    public double getDps() {
        return dps;
    }

    public void setDps(double dps) {
        this.dps = dps;
    }

    public PrimaryAttributes getBasePrimaryAttributes() {
        return basePrimaryAttributes;
    }

    public void setBasePrimaryAttributes(PrimaryAttributes basePrimaryAttributes) {
        this.basePrimaryAttributes = basePrimaryAttributes;
    }

    public PrimaryAttributes getIncrementPrimaryAttributes() {
        return incrementPrimaryAttributes;
    }

    public void setIncrementPrimaryAttributes(PrimaryAttributes incrementPrimaryAttributes) {
        this.incrementPrimaryAttributes = incrementPrimaryAttributes;
    }

    public SecondaryAttributes getBaseSecondaryAttributes() {
        return baseSecondaryAttributes;
    }

    public void setBaseSecondaryAttributes(SecondaryAttributes baseSecondaryAttributes) {
        this.baseSecondaryAttributes = baseSecondaryAttributes;
    }

    public Hashtable<Item.Slot, Item> getEquipment() {
        return equipment;
    }

    public void setEquipment(Hashtable<Item.Slot, Item> equipment) {
        this.equipment = equipment;
    }
}
