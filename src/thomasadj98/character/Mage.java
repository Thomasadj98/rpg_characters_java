package thomasadj98.character;

import thomasadj98.exceptions.*;
import thomasadj98.item.*;

public class Mage extends Character {

    public Mage(String name) {
        super(name, 1, 1, 8, 5);
        System.out.println("Mage created successfully!");

        PrimaryAttributes incrementPrimaryAttributes = new PrimaryAttributes(3, 1, 1, 5);
        this.setIncrementPrimaryAttributes(incrementPrimaryAttributes);
    }

    @Override
    public String equip(Weapon weapon) throws InvalidWeaponException {
        if (weapon.getLevelToEquip() > this.getLevel()) {
            throw new InvalidWeaponException("Your Character's level is not high enough.");
        }
        if (weapon.getWeaponType() != Weapon.weaponType.STAFF_WEAPON && weapon.getWeaponType() != Weapon.weaponType.WAND_WEAPON) {
            throw new InvalidWeaponException("Your Character can't equip this type of weapon.");
        }
        equipment.put(Item.Slot.WEAPON_SLOT, weapon);

        return "New weapon equipped!";
    }

    @Override
    public String equip(Armor armor) throws InvalidArmorException {
        if (armor.getLevelToEquip() > this.getLevel()) {
            throw new InvalidArmorException("Your Character's level is not high enough to equip this type of armor.");
        }
        if (armor.getArmorType() != Armor.armorType.CLOTH_ARMOR) {
            throw new InvalidArmorException("Your Character can't equip this type of armor.");
        }

        equipment.put(armor.getItemSlot(), armor);

        return "New armor equipped!";
    }

    @Override
    public void calculateDPS() {

    }
}
