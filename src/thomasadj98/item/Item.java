package thomasadj98.item;

public abstract class Item {

    public Item(String name, int levelToEquip, Slot itemSlot) {
        this.name = name;
        this.levelToEquip = levelToEquip;
        this.itemSlot = itemSlot;
    }

    public enum Slot {
        HEAD_SLOT,
        BODY_SLOT,
        LEGS_SLOT,
        WEAPON_SLOT
    }

    private String name;
    private int levelToEquip;
    private Slot itemSlot;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getLevelToEquip() {
        return levelToEquip;
    }

    public void setLevelToEquip(int levelToEquip) {
        this.levelToEquip = levelToEquip;
    }

    public Slot getItemSlot() {
        return itemSlot;
    }

    public void setItemSlot(Slot itemSlot) {
        this.itemSlot = itemSlot;
    }
}
