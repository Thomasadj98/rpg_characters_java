package thomasadj98.item;

public class Weapon extends Item {

    public Weapon(String name, int levelToEquip, Slot itemSlot, Weapon.weaponType weaponType, WeaponAttributes weaponAttributes) {
        super(name, levelToEquip, itemSlot);
        this.weaponType = weaponType;
        this.weaponAttributes = weaponAttributes;
    }

    public enum weaponType {
        AXE_WEAPON,
        BOW_WEAPON,
        DAGGER_WEAPON,
        HAMMER_WEAPON,
        STAFF_WEAPON,
        SWORD_WEAPON,
        WAND_WEAPON
    }

    private weaponType weaponType;
    private WeaponAttributes weaponAttributes;

    
    public Weapon.weaponType getWeaponType() {
        return weaponType;
    }

    public void setWeaponType(Weapon.weaponType weaponType) {
        this.weaponType = weaponType;
    }

    public WeaponAttributes getWeaponAttributes() {
        return weaponAttributes;
    }

    public void setWeaponAttributes(WeaponAttributes weaponAttributes) {
        this.weaponAttributes = weaponAttributes;
    }
}
