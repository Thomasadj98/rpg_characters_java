package thomasadj98.item;

import thomasadj98.character.PrimaryAttributes;

public class Armor extends Item {
    public Armor(String name, int levelToEquip, Slot itemSlot, Armor.armorType armorType, PrimaryAttributes attributes) {
        super(name, levelToEquip, itemSlot);
        this.armorType = armorType;
        this.attributes = attributes;
    }

    public enum armorType {
        CLOTH_ARMOR,
        LEATHER_ARMOR,
        MAIL_ARMOR,
        PLATE_ARMOR
    }

    private armorType armorType;
    private PrimaryAttributes attributes;


    public Armor.armorType getArmorType() {
        return armorType;
    }

    public void setArmorType(Armor.armorType armorType) {
        this.armorType = armorType;
    }

    public PrimaryAttributes getAttributes() {
        return attributes;
    }

    public void setAttributes(PrimaryAttributes attributes) {
        attributes = attributes;
    }
}
