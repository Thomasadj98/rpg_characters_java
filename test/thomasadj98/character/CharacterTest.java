package thomasadj98.character;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class CharacterTest {

    @Test
    void isCharacterCorrectLevel_characterName_lvlOneCharacter() {
        Ranger newChar = new Ranger("Clearance");
        assertEquals(1, newChar.getLevel());
    }

    @Test
    void doesCharacterLevelUp_levelOne_levelTwoChar() {
        Ranger rangerCharacter = new Ranger("Robin");
        rangerCharacter.levelUp(1);
        assertEquals(2, rangerCharacter.getLevel());
    }


    // Check if proper Primary Attribute values are set on initialization

    // Mage
    @Test
    void setCorrectPrimaryAttributesMage_characterInitialize_primaryAttributes() {
        Mage mageCharacter = new Mage("Mage");
        PrimaryAttributes expectedValues = new PrimaryAttributes(1, 1, 8, 5);
        PrimaryAttributes actualValues = mageCharacter.getBasePrimaryAttributes();
        boolean assertion = expectedValues.equals(actualValues);
        assertTrue(assertion);
    }

    // Ranger
    @Test
    void setCorrectPrimaryAttributesRanger_characterInitialize_primaryAttributes() {
        Ranger rangerCharacter = new Ranger("Ranger");
        PrimaryAttributes expectedValues = new PrimaryAttributes(2, 6, 1, 8);
        PrimaryAttributes actualValues = rangerCharacter.getBasePrimaryAttributes();
        assertEquals(expectedValues, actualValues);
    }

    // Rogue
    @Test
    void setCorrectPrimaryAttributesRogue_characterInitialize_primaryAttributes() {
        Rogue rogueCharacter = new Rogue("Rogue");
        PrimaryAttributes expectedValues = new PrimaryAttributes(2, 6, 1, 8);
        PrimaryAttributes actualValues = rogueCharacter.getBasePrimaryAttributes();
        assertEquals(expectedValues, actualValues);
    }

    // Warrior
    @Test
    void setCorrectPrimaryAttributesWarrior_characterInitialize_primaryAttributes() {
        Warrior warriorCharacter = new Warrior("Warrior");
        PrimaryAttributes expectedValues = new PrimaryAttributes(5, 2, 1, 10);
        PrimaryAttributes actualValues = warriorCharacter.getBasePrimaryAttributes();
        assertEquals(expectedValues, actualValues);
    }


    // Check if Primary Attributes are properly calculated when Character levels up

    // Mage
    @Test
    void setCorrectPrimaryAttributesMage_levelUp_updatedPrimaryAttributes() {
        Mage mageCharacter = new Mage("Mage");
        PrimaryAttributes expectedValues = new PrimaryAttributes(2, 13, 2, 8);
        mageCharacter.levelUp(1);
        PrimaryAttributes actualValues = mageCharacter.getBasePrimaryAttributes();
        assertEquals(expectedValues, actualValues);
    }

    // Ranger
    @Test
    void setCorrectPrimaryAttributesRanger_levelUp_updatedPrimaryAttributes() {
        Ranger rangerCharacter = new Ranger("Ranger");
        PrimaryAttributes expectedValues = new PrimaryAttributes(2, 12, 2, 10);
        rangerCharacter.levelUp(1);
        PrimaryAttributes actualValues = rangerCharacter.getBasePrimaryAttributes();
        assertEquals(expectedValues, actualValues);
    }

    // Mage
    @Test
    void setCorrectPrimaryAttributesRogue_levelUp_updatedPrimaryAttributes() {
        Rogue rogueCharacter = new Rogue("Rogue");
        PrimaryAttributes expectedValues = new PrimaryAttributes(3, 10, 2, 11);
        rogueCharacter.levelUp(1);
        PrimaryAttributes actualValues = rogueCharacter.getBasePrimaryAttributes();
        assertEquals(expectedValues, actualValues);
    }

    // Mage
    @Test
    void setCorrectPrimaryAttributesWarrior_levelUp_updatedPrimaryAttributes() {
        Warrior warriorCharacter = new Warrior("Warrior");
        PrimaryAttributes expectedValues = new PrimaryAttributes(8, 4, 2, 15);
        warriorCharacter.levelUp(1);
        PrimaryAttributes actualValues = warriorCharacter.getBasePrimaryAttributes();
        assertEquals(expectedValues, actualValues);
    }


    /// Check Secondary Stats of leveled up character (Warrior)

    // Warrior
    @Test
    void setCorrectSecStatsWarrior_levelUp_updatedSecStats() {
        Warrior warriorCharacter = new Warrior("Warrior");
        SecondaryAttributes expectedValues = new SecondaryAttributes(150, 12, 2);
        warriorCharacter.levelUp(1);
        SecondaryAttributes actualValues = warriorCharacter.getBaseSecondaryAttributes();
        assertEquals(expectedValues, actualValues);
    }
}