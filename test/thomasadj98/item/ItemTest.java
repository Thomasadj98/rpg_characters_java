package thomasadj98.item;

import org.junit.jupiter.api.Test;
import thomasadj98.character.*;
import thomasadj98.exceptions.*;

import static org.junit.jupiter.api.Assertions.*;

class ItemTest {

    Weapon testAxeWeapon = new Weapon(
            "Axy",
            1,
            Item.Slot.WEAPON_SLOT,
            Weapon.weaponType.AXE_WEAPON,
            new WeaponAttributes(
                    7,
                    1
            )
    );

    Armor testPlateArmor = new Armor(
        "Plate Body",
        1,
        Item.Slot.BODY_SLOT,
        Armor.armorType.PLATE_ARMOR.PLATE_ARMOR,
        new PrimaryAttributes(
                1,
                0,
                0,
                2
            )
    );

    Weapon testBowWeapon = new Weapon(
            "gang",
            1,
            Item.Slot.WEAPON_SLOT,
            Weapon.weaponType.BOW_WEAPON,
            new WeaponAttributes(
                    12,
                    0.8
            )
    );

    Armor testClothArmor = new Armor(
            "Plate Body",
            1,
            Item.Slot.HEAD_SLOT,
            Armor.armorType.PLATE_ARMOR.CLOTH_ARMOR,
            new PrimaryAttributes(
                    0,
                    0,
                    5,
                    1
            )
    );

    @Test
    void equipLvlTwoWeaponToLvlOneChar_LvlTwoWeapon_InvalidWeaponException() throws InvalidWeaponException {
        Warrior warriorChar = new Warrior("Warrior");
        testAxeWeapon.setLevelToEquip(2);
        assertThrows(InvalidWeaponException.class, () -> warriorChar.equip(testAxeWeapon));
    }

    @Test
    void characterEquipsHighLevelArmor_lvlTwoArmor_ThrowInvalidArmorException() {
        Warrior warriorChar = new Warrior("Warrior");
        testPlateArmor.setLevelToEquip(2);
        assertThrows(InvalidArmorException.class, () -> warriorChar.equip(testPlateArmor));
    }

    @Test
    void characterEquipsWrongWeaponType_wrongWeaponType_InvalidWeaponException() {
        Warrior warriorChar = new Warrior("Warrior");
        assertThrows(InvalidWeaponException.class, () -> warriorChar.equip(testBowWeapon));
    }

    @Test
    void characterEquipsWrongArmorType_wrongArmorType_InvalidArmorException() {
        Warrior warriorChar = new Warrior("Warrior");
        assertThrows(InvalidArmorException.class, () -> warriorChar.equip(testClothArmor));
    }

    @Test
    void characterEquipsValidWeaponType_correctWeaponType_successMessage() throws InvalidWeaponException {
        Warrior warriorChar = new Warrior("Warrior");
        String expected = "New weapon equipped!";
        String actual = warriorChar.equip(testAxeWeapon);
        assertEquals(expected, actual);
    }

    @Test
    void characterEquipsValidArmorType_correctArmorType_successMessage() throws InvalidArmorException {
        Warrior warriorChar = new Warrior("Warrior");
        String expected = "New armor equipped!";
        String actual = warriorChar.equip(testPlateArmor);
        assertEquals(expected, actual);
    }

}